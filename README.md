# books-collector

This project is used to populate a database on MongoDB with book objects.

The tool performs the following tasks:

- It retrieves a list of New York Times best-seller books (per type) by calling NYT Books API
- Based on available ISBN values, another API service gets called to retrieve the number of words per book
- Combining data received from both external services, new Book objects get created and inserted into dedicated collections

Each list of books (categorized by type) gets stored in a separate collection located on the same database.

```json
{
    "primary_isbn10": "014313356X",
    "title": "NO ONE IS TOO SMALL TO MAKE A DIFFERENCE",
    "author": "Greta Thunberg",
    "PagesCount": 112,
    "WordsCount": 16095,
    "AvgReadingTimePerHour": 1.07,
    "Rank": 2
}
````

Type can be passed as arguments to the application. See variables that can run the application with:

| Variable | Description        | Example value                        |                           |
| ----------------------------- | ------------------------------------ | ------------------------- |
| BOOKS_COLLECTOR_MONGO_URI     | Mongo connection string              | mongodb://localhost:27017 |
| BOOKS_COLLECTOR_MONGO_DB      | Mongo database name                  | titalib                   |
| BOOKS_COLLECTOR_BOOK_TYPES    | book type(s) used by NYT             | hardcover-nonfiction      |
| BOOKS_COLLECTOR_NYTAPI_KEY    | NYT API Key with access to Books API | A_NICE_API_KEY            |

## usage:

### Locally (a running MongoDB is needed):

Run `go run main.go --nytapi-key $NYT_API_KEY` 

_(note: here we're using default values for other variables described above)_

### Locally (docker-compose):

Run `docker-compose up` and connect to the database to verify book objects have been created. Please see: https://gitlab.com/azizos/titalib/books

## Why?

We're using Books Collector to populate the TitaLib database with books needed to be used by the TitaLib's Books Service. TitaLib is a microservices application used to allow Titanic survivors to burrow books. Duplicate books will not be added.