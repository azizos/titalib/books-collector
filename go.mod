module gitlab.com/azizos/titalib/books-collector

go 1.13

require (
	github.com/ardanlabs/conf v1.2.1
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/pkg/errors v0.9.1 // indirect
	github.com/stretchr/testify v1.5.1 // indirect
	github.com/tidwall/gjson v1.6.0
	go.mongodb.org/mongo-driver v1.3.1
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
