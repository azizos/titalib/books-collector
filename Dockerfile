FROM golang:1.13 as build_collector
ENV CGO_ENABLED 0

RUN mkdir -p /app

WORKDIR /app
COPY go.* ./
COPY collector collector

WORKDIR /app/collector/
RUN go build

FROM alpine:3.7

COPY --from=build_collector /app/collector/collector /app/main

WORKDIR /app
CMD /app/main