package utils

import (
	"context"
	"time"

	"github.com/op/go-logging"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var log, _ = GetLogModule()

// ConnectDB makes connection with the database
func ConnectDB(mongoURI string, mongoDB string) (*mongo.Database, context.Context) {
	log.Info("Connecting to the database..")
	ctx, _ := context.WithTimeout(context.Background(), 120*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(mongoURI))
	if err != nil {
		log.Panicf("Error connecting to database: %v", err)
	}
	database := client.Database(mongoDB)
	return database, ctx
}

// GetLogModule is used to create and return a formatted logging module to be used globally in app
func GetLogModule() (*logging.Logger, error) {
	var format = logging.MustStringFormatter(
		`%{color}%{time:15:04:05.000} %{shortfunc} ▶ %{level:.6s} %{id:03x}%{color:reset} %{message}`,
	)
	logging.SetFormatter(format)
	return logging.GetLogger("app")
}
