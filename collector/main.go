package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math"
	"net/http"
	"os"
	"strings"
	"time"

	"gitlab.com/azizos/titalib/books-collector/collector/utils"
	"gopkg.in/mgo.v2/bson"

	"go.mongodb.org/mongo-driver/mongo"

	"github.com/ardanlabs/conf"
	"github.com/tidwall/gjson"
)

var dbCollectionName string = "books"
var log, _ = utils.GetLogModule()
var db *mongo.Database
var ctx context.Context

func main() {
	var cfg struct {
		MongoURI  string   `conf:"default:mongodb://localhost:27017"`
		MongoDB   string   `conf:"default:titalib"`
		BookTypes []string `conf:"default:hardcover-nonfiction;hardcover-fiction"`
		NYTAPIKey string   `conf:"default:MY_KEY"`
	}
	if err := conf.Parse(os.Args[1:], "BOOKS_COLLECTOR", &cfg); err != nil {
		// note: returning help instructions
		if err == conf.ErrHelpWanted {
			usage, err := conf.Usage("BOOKS_COLLECTOR", &cfg)
			if err != nil {
				log.Fatalf("Generating config usage %s\n", err)
			}
			fmt.Println(usage)
		}
		if err != nil {
			log.Fatalf("Parsing configs %s\n", err)
		}
	}

	db, ctx = utils.ConnectDB(cfg.MongoURI, cfg.MongoDB)
	for _, s := range cfg.BookTypes {
		storeBestSellers(s, cfg.NYTAPIKey)
	}
}

func storeBestSellers(bookType string, nytAPIKey string) {
	books, err := retrieveBooksPerType(bookType, nytAPIKey)
	if err != nil {
		log.Fatalf("Retrieving best-seller books failed with error %s\n", err)
	}
	collection := db.Collection(dbCollectionName)
	log.Infof("Adding books to collection '%v'..", bookType)
	for _, book := range books {
		if !bookExists(book.ISBN) {
			bookWordCount, bookPagesCount := retrieveBookDetails(book.ISBN)
			if err != nil {
				log.Fatalf("Failed adding alovelace: %v", err)
			}
			book.PagesCount = bookPagesCount
			book.WordsCount = bookWordCount
			book.AvgReadingTimePerHour = calculateReadingHours(bookWordCount, 250)
			book.BookType = bookType
			book.CreationDate = time.Now().Format("2006-01-02")
			log.Infof("Inserting '%v'", book.Title)
			_, err = collection.InsertOne(ctx, &book)
			if err != nil {
				log.Fatalf("Failed adding alovelace: %v", err)
			}
			if err != nil {
				log.Fatalf("Failed adding word and pages count for a book: %v", err)
			}
		}
	}
}

func retrieveBookDetails(isbn string) (int32, int32) {
	url := "https://yoga.readinglength.com"
	payload := fmt.Sprintf("[{\"query\": \"query WORDCOUNT_QUERY($isbn: String!) {wordCounts(where: {isbn10: $isbn}) {wordCount}}\",\"variables\": {\"isbn\":\"%[1]s\"}},{\"query\": \"query BOOK_FROM_ISBN_QUERY($isbn: String!) {findBook(isbn10: $isbn) {pageCount }}\",\"variables\":{ \"isbn\": \"%[1]s\"}}]", isbn)

	req, err := http.NewRequest("POST", url, strings.NewReader(payload))
	req.Header.Add("Content-Type", "application/json")
	client := &http.Client{}
	response, err := client.Do(req)
	if err != nil || response.StatusCode != 200 {
		log.Fatalf("The HTTP request failed with error: ", response.Body)
	}
	defer response.Body.Close()
	responseBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}

	wordCount := int32(gjson.Get(string(responseBody), "0.data.wordCounts.0.wordCount").Int())
	pagesCount := int32(gjson.Get(string(responseBody), "1.data.findBook.pageCount").Int())

	if wordCount == 0 && pagesCount != 0 {
		wordCount = pagesCount * 250
	}

	return wordCount, pagesCount
}

func retrieveBooksPerType(bookType string, apiKey string) ([]Book, error) {
	var responseData NYTBooksResponse
	url := fmt.Sprintf("https://api.nytimes.com/svc/books/v3/lists/current/%s.json?api-key=%s", bookType, apiKey)
	response, err := http.Get(url)
	if err != nil || response.StatusCode != 200 {
		log.Fatalf("The HTTP request failed with error: ", response.StatusCode)
	}
	responseBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}

	err = json.Unmarshal(responseBody, &responseData)
	if err != nil {
		log.Fatal(err)
	}
	return responseData.Results.Books, err
}

func bookExists(isbn string) bool {
	collection := db.Collection(dbCollectionName)
	err := collection.FindOne(ctx, bson.M{"primary_isbn10": isbn}).Err()
	if err == nil {
		log.Infof("Book '%v' already exists in the database and cannot be added", isbn)
		return true
	}
	return false
}

func calculateReadingHours(wordCount int32, avgReadWordsPerMin int32) float64 {
	return math.Round(float64(wordCount)/float64(avgReadWordsPerMin)/60*100) / 100
}

// NYTBooksResponse is representing the root node returned by NYC Book API response
type NYTBooksResponse struct {
	Results Results `json:"results"`
}

// Results is a JSON node included in the NYT Book API response
type Results struct {
	Books []Book `json:"books"`
}

// Book represents book objects
type Book struct {
	ID                    bson.ObjectId `json:"_id,omitempty" bson:"_id,omitempty" `
	ISBN                  string        `json:"primary_isbn10" bson:"primary_isbn10"`
	Title                 string        `json:"title" bson:"title"`
	Author                string        `json:"author" bson:"author"`
	CreationDate          string        `json:"added_on" bson:"added_on"`
	PagesCount            int32         `json:"pages_count" bson:"pages_count"`
	WordsCount            int32         `json:"words_count" bson:"words_count"`
	AvgReadingTimePerHour float64       `json:"avg_reading_time_per_hour" bson:"avg_reading_time_per_hour"`
	BookType              string        `json:"type" json:"type"`
}
